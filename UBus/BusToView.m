//
//  BusToView.m
//  UBus
//
//  Created by Patong Vang on 5/1/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "BusToView.h"

@implementation BusToView
{
    BusToView *anotherSingle;
}

@synthesize megabus;
@synthesize coach;

+ (BusToView *) singleObj{
    static BusToView *single = nil;
    
    @synchronized(self){
        if(!single){
            single = [[BusToView alloc] init];
        }
    }
    
    return single;
}

@end
