//
//  EditBusViewController.m
//  UBus
//
//  Created by Patong Vang on 4/29/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "EditBusViewController.h"

@interface EditBusViewController ()

@end

@implementation EditBusViewController



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    singleton = [Singleton singleton];
  
    self.switchMegabus.on = singleton.megabus;
    
    self.switchCoach.on = singleton.coach;
    
     //self.switchMegabus.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"megabusSwitch"];
    
    
    //self.switchCoach.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"coachSwitch"];
    
    
    NSLog(@"On opening, megabus: %d, coach switch: %d", self.switchMegabus.on, self.switchCoach.on );
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)toggleMegabus:(id)sender {

    
     [[NSUserDefaults standardUserDefaults] setBool:self.switchMegabus.on forKey:@"megabusSwitch"];
    
    
}

- (IBAction)toggleCoach:(id)sender {
     [[NSUserDefaults standardUserDefaults] setBool:self.switchCoach.on forKey:@"coachSwitch"];
}

- (void) viewWillDisappear:(BOOL) animated
{

    singleton.megabus = self.switchMegabus.on;
    singleton.coach = self.switchCoach.on;
    
        NSLog(@"On closing, megabus: %d, coach switch: %d", self.switchMegabus.on, self.switchCoach.on );
  
}



@end
