//
//  StopDetailsPopoverViewController.m
//  UBus
//
//  Created by Cindy on 5/7/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "StopDetailsPopoverViewController.h"
#import "StopCell.h"

@interface StopDetailsPopoverViewController ()

@end

@implementation StopDetailsPopoverViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        CGRect cell = CGRectMake(41, 11, 199, 30);
        
        //Calculate how tall the view should be by multiplying 
        //the individual row height by the total number of rows.
        NSInteger rowsCount = 3;
        NSInteger singleRowHeight = 60;
        NSInteger totalRowsHeight = rowsCount * singleRowHeight;
        
        //Calculate how wide the view should be by finding how 
        //wide each string is expected to be
        CGFloat largestLabelWidth = cell.size.width;
        
        //Add a little padding to the width
        CGFloat popoverWidth = largestLabelWidth + 77;
        
        //Set the property to tell the popover container how big this view will be.
        self.preferredContentSize = CGSizeMake(popoverWidth, totalRowsHeight);

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_stops count];
}


- (StopCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //StopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stopCell" forIndexPath:indexPath];
    //if(cell == nil) {
    StopCell *cell = [[StopCell alloc] init];
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StopCell" owner:self options:nil];
    cell = [topLevelObjects objectAtIndex:0];
    //}
    
    if([_stops objectAtIndex:indexPath.row] != nil){
        
        NSDictionary *dict = [_stops objectAtIndex:indexPath.row];
        [cell.locationLabel setText:[dict objectForKey:@"location"]];
        [cell.addressLabel setText:[dict objectForKey:@"address"]];
        [cell.arriveTimeLabel setText:[dict objectForKey:@"arriveTime"]];
        [cell.departTimeLabel setText:[dict objectForKey:@"departTime"]];
        
        cell.addressLabel.numberOfLines = 2;
        [cell.addressLabel setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return cell;
}

@end
