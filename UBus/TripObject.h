//
//  TripObject.h
//  UBus
//
//  Created by Cindy on 4/8/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TripObject : NSObject

@property double price;
@property int duration;
@property int departTime;
@property int arriveTime;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *departLocation;
@property (strong, nonatomic) NSString *arriveLocation;
@property (strong, nonatomic) NSString *departAddress;
@property (strong, nonatomic) NSString *arriveAddress;

//array of dictionary of stops and times
@property (strong, nonatomic) NSMutableArray *stops;
@property (strong, nonatomic) NSNumber *numStops;

@property (strong, nonatomic) NSString *note;   //need implementation
@property (strong, nonatomic) NSURL *buyURL;    //need implementation

@property int carrierID;
@property BOOL needReservation;


/* Class and Helper Functions */
- (NSString *) stringDepartTime;
- (NSString *) stringArriveTime;
- (NSString *) stringDuration;
- (NSString *) stringPrice;

- (void) setDepartTime:(NSString *)departTime andArriveTime:(NSString *)arriveTime;
- (void) setPriceFrom:(NSString *)price;

- (NSComparisonResult)comparePrice:(TripObject *)anotherTrip;
- (NSComparisonResult)compareDuration:(TripObject *)anotherTrip;
- (NSComparisonResult)compareDepartTime:(TripObject *)anotherTrip;
- (NSComparisonResult)compareArriveTime:(TripObject *)anotherTrip;
- (BOOL)compare:(TripObject *)anotherTrip;
@end
