//
//  MyTripsTableViewController.h
//  UBus
//
//  Created by Cindy on 5/10/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "UIPopoverController+iPhone.h"
#import "SearchResultViewController.h"
#import "TripDetailsViewController.h"
#import "SortPopoverViewController.h"
#import "TabBarController.h"

//#import "MBProgressHUD.h"
#import "TripCell.h"
#import "TripObject.h"
#import "TFHpple.h"

@interface MyTripsTableViewController : UITableViewController 

@end
