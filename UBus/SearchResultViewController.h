//
//  SearchResultViewController.h
//  UBus
//
//  Created by Cindy on 4/13/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationObject.h"
#import "SortPopoverViewController.h"
#import "Singleton.h"

@interface SearchResultViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SortPickerDelegate>{
    Singleton *singleton;
}

//sort function popover
@property (nonatomic, strong) NSNumber *sort;
@property (nonatomic, strong) SortPopoverViewController *sortPicker;
@property (nonatomic, strong) UIPopoverController *sortPickerPopover;

//views
@property (strong, nonatomic) IBOutlet UIView *bar;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *noTripsView;

//Labels and button
@property (weak, nonatomic) IBOutlet UILabel *noTripsLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sortButton;
@property (weak, nonatomic) IBOutlet UIButton *incrementDateButton;
@property (weak, nonatomic) IBOutlet UIButton *decrementDateButton;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *departLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocationLabel;

//trip array for search result
@property (strong, nonatomic) NSMutableArray *searchedTrips;

//trip information
@property (strong, nonatomic) LocationObject *departLocation;
@property (strong, nonatomic) LocationObject *arriveLocation;
@property (strong, nonatomic) NSDate *date;


@end
