//
//  SortPopoverViewController.h
//  UBus
//
//  Created by Cindy on 4/23/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PRICELOWTOHIGH 1
#define PRICEHIGHTOLOW 2
#define DURATIONLOWTOHIGH 3
#define DURATIONHIGHTOLOW 4
#define DEPARTLOWTOHIGH 5
#define DEPARTHIGHTOLOW 6
#define ARRIVELOWTOHIGH 7
#define ARRIVEHIGHTOLOW 8
    
@protocol SortPickerDelegate <NSObject>
@required
-(void)selectedSort:(NSNumber *)newSort;
@end

@interface SortPopoverViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *sortFunctions;
@property (nonatomic, weak) id<SortPickerDelegate> delegate;



@end
