//
//  TripViewController.m
//  UBus
//
//  Created by Cindy on 4/24/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "TripViewController.h"
#import "StopCell.h"


@interface TripViewController () 
@end

@implementation TripViewController{
     NSDateFormatter *formatter;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _displayingFront = true;
    
    //init date formatter
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    
    [self loadViews];
    
    /* Set up map view */
    [_mapSegment addTarget:self
                    action:@selector(pick:)
          forControlEvents:UIControlEventValueChanged];
    _address.numberOfLines = 3;
    [_address setLineBreakMode:NSLineBreakByWordWrapping];
    [self loadMap:[NSString stringWithFormat:@"%@,%@", self.trip.departAddress, self.trip.departLocation]];
    
    //set up recognization of single tap
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(flipView:)];
    [self.view addGestureRecognizer:singleFingerTap];
}

- (void)loadViews{
    /* Set up info view */
    _departAddress.numberOfLines = 3;
    [_departAddress setLineBreakMode:NSLineBreakByWordWrapping];
    _arriveAddress.numberOfLines = 3;
    [_arriveAddress setLineBreakMode:NSLineBreakByWordWrapping];
    
    [self.departTime setText:[self.trip stringDepartTime]];
    [self.arrivalTime setText:[self.trip stringArriveTime]];
    [self.departLocation setText:[self.trip departLocation]];
    [self.arriveLocation setText:[self.trip arriveLocation]];
    [self.departAddress setText:self.trip.departAddress];
    [self.arriveAddress setText:self.trip.arriveAddress];
    [self.date setText:[formatter stringFromDate:self.trip.date]];
    [self.price setText:[self.trip stringPrice]];
    [self.duration setText:[self.trip stringDuration]];
    
    if(self.trip.needReservation == YES)
        [self.reservation setText:@"Reservation Required"];
    else
        [self.reservation setText:@"Reservation Not Required"];
    
    switch(_trip.carrierID){
        case 1:
            [self.carrierImage setImage:[UIImage imageNamed:@"megabus"]];
            break;
        case 2:
            [self.carrierImage setImage:[UIImage imageNamed:@"coachusa"]];
            break;
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)flipView:(UITapGestureRecognizer *)recognizer {    
    [UIView transitionWithView:self.view duration:1.0 
                       options:(self.displayingFront ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        if(self.displayingFront){
                            self.infoView.hidden = true;
                            self.mapView.hidden = false;
                        }
                        else{
                            self.infoView.hidden = false;
                            self.mapView.hidden = true;
                        }
                    }
                    completion:^(BOOL finished) {
                        if (finished) {
                            self.displayingFront = !self.displayingFront;
                        }
                    }];
}


-(void) pick:(id)sender
{
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    if([segmentedControl selectedSegmentIndex] == 0)
        [self loadMap:[NSString stringWithFormat:@"%@, %@", _trip.departAddress, _trip.departLocation]];
    else
        [self loadMap:[NSString stringWithFormat:@"%@, %@", _trip.arriveAddress, _trip.arriveLocation]];
} 


-(void) loadMap:(NSString *)address
{
    [_address setText:address];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            if (placemarks && placemarks.count > 0) {
                CLPlacemark *topResult = [placemarks objectAtIndex:0];
                MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                
                MKCoordinateRegion region = self.map.region;  // this is causing a zoom problem
                region.center = placemark.location.coordinate;
                region.span.longitudeDelta /= 100.0;
                region.span.latitudeDelta /= 100.0;
                
                [self.map setRegion:region animated:YES];
                [self.map addAnnotation:placemark];
            }
        }
    }];
}

- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender {
    TripDetailsViewController *parentController = (TripDetailsViewController *)self.parentViewController;
    if(parentController.index < [parentController.tripList count] - 1) {
        self.trip = [parentController.tripList objectAtIndex:parentController.index + 1];
        parentController.trip = [parentController.tripList objectAtIndex:parentController.index + 1];
        parentController.index += 1;
    }
    
    [self loadViews];
}

- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender {
    TripDetailsViewController *parentController = (TripDetailsViewController *) self.parentViewController;
    if(parentController.index > 0) {
        self.trip = [parentController.tripList objectAtIndex:parentController.index - 1];
        parentController.trip = [parentController.tripList objectAtIndex:parentController.index - 1];
        parentController.index -= 1;
    }
    
    [self loadViews];
}
@end
