//
//  SearchResultViewController.m
//  UBus
//
//  Created by Cindy on 4/13/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "UIPopoverController+iPhone.h"
#import "SearchResultViewController.h"
#import "TripDetailsViewController.h"
#import "SortPopoverViewController.h"
#import "TabBarController.h"

#import "MBProgressHUD.h"
#import "TripCell.h"
#import "TripObject.h"
#import "TFHpple.h"

@interface SearchResultViewController(){
    
    //NSURLSession vars
    NSURLSession *session;
    NSURLSessionDataTask *task;
    
    //TFHpple vars
    TFHpple *parser;
    NSString *query;
    NSArray *nodes;
    
    //Date Formatter
    NSDateFormatter *formatter;
    
    //Dictionaries
    NSDictionary *coachUSAFares;
    
    BOOL tripsFound;
    
}
@end


@implementation SearchResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.table = [self.table init];
        self.bar = [self.bar init];
    }
    return self;
}

-(void) initAll
{
//    tripsFound = NO;
//    self.noTripsView.hidden = YES;
    
    //init session
    session = [NSURLSession sharedSession];
    
    //init loading bar
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading Trips";
    [hud hide:YES afterDelay:5];
    
    
    //init trips
    TabBarController *tabBar = (TabBarController *)self.tabBarController;
    self.searchedTrips = [[NSMutableArray alloc] init];
    self.date = tabBar.date;
    self.departLocation = tabBar.departLocation;
    self.arriveLocation = tabBar.arriveLocation;
    
    //init date formatter
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    
    //set labels
    [self.dateLabel setText:[formatter stringFromDate:self.date]];
    [self.departLocationLabel setText:[NSString stringWithFormat:@"%@, %@", self.departLocation.city, self.departLocation.state]];
    [self.arriveLocationLabel setText:[NSString stringWithFormat:@"%@, %@", self.arriveLocation.city, self.arriveLocation.state]];
    
    //coach usa fares
    coachUSAFares = @{
                      @"Madison": @{
                              @"Chicago Midway Airport": @"32",
                              @"Chicago O'Hare Airport": @"30",
                              @"Janesville": @"8",
                              @"South Beloit": @"9",
                              @"Rockford": @"11",
                              @"Chicago": @"30"
                              } ,
                      @"Janesville": @{
                              @"Chicago Midway Airport": @"31",
                              @"Chicago O'Hare Airport": @"29",
                              @"Madison": @"8",
                              @"South Beloit": @"6",
                              @"Rockford": @"10",
                              @"Chicago": @"29"
                              },
                      @"South Beloit": @{
                              @"Chicago Midway Airport": @"30",
                              @"Chicago O'Hare Airport": @"28",
                              @"Madison": @"9",
                              @"Janesville": @"6",
                              @"Rockford": @"9",
                              @"Chicago": @"28"
                              },
                      @"Rockford": @{
                              @"Chicago Midway Airport": @"25",
                              @"Chicago O'Hare Airport": @"21",
                              @"Madison": @"11",
                              @"Janesville": @"10",
                              @"South Beloit": @"9",
                              @"Chicago": @"22"
                              },
                      @"Chicago": @{
                              @"Madison": @"30",
                              @"Janesville": @"29",
                              @"South Beloit": @"28",
                              @"Rockford": @"22"
                              },
                      @"Chicago Midway Airport": @{
                              @"Madison": @"32",
                              @"Janesville": @"31",
                              @"South Beloit": @"30",
                              @"Rockford": @"25"
                              },
                      @"Chicago O'Hare Airport": @{
                              @"Madison": @"30",
                              @"Janesville": @"29",
                              @"South Beloit": @"28",
                              @"Rockford": @"21"
                              }
                      };
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    singleton = [Singleton singleton];
    [self initAll];

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self loadTrips];
    });

    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if([self.searchedTrips count] == 0){
//            self.noTripsView.hidden = false;
//            
//        }
//        else 
//            [self.table reloadData];
//         [MBProgressHUD hideHUDForView:self.view animated:YES];
//    });
   
}

- (void) loadTrips
{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(singleton.megabus){
        [self getMegabusTrips];
    }
    
    if(singleton.coach){
        [self getCoachusaTrips];
    }
}

- (void)getMegabusTrips
{
    if([self.searchedTrips count] > 0)
        [self.searchedTrips removeAllObjects];
    
    NSURL *url = [NSURL URLWithString:
                  [NSString stringWithFormat:@"http://us.megabus.com/JourneyResults.aspx?originCode=%@&destinationCode=%@&outboundDepartureDate=%@&passengerCount=1",[self.departLocation.megabusCode stringValue], [self.arriveLocation.megabusCode stringValue], [formatter stringFromDate:self.date]]];
    task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
            {
                //load parser with html
                parser = [TFHpple hppleWithHTMLData:data];
                query = @"//div[@class='JourneyList']/ul[@class='journey standard none']";
                nodes = [parser searchWithXPathQuery:query];
                
                for (TFHppleElement *element in nodes) {
                    TripObject *trip = [[TripObject alloc] init];
                    trip.stops = [[NSMutableArray alloc] init];
                    [self.searchedTrips addObject:trip];
                    
                    //load trip time, location, stops info
                    query = @"//li[@class='six']/a";
                    NSArray *infoNode = [element searchWithXPathQuery:query];
                    NSURL *tableUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://us.megabus.com/%@",[[infoNode objectAtIndex:0] objectForKey:@"href"]]];
                    NSData *tableData = [NSData dataWithContentsOfURL:tableUrl];
                    TFHpple *tableParser = [TFHpple hppleWithHTMLData:tableData];
                    NSString *tableXpathQueryString = @"//div[@class='schedule_details']/div/table/tr";
                    NSArray *tableNodes = [tableParser searchWithXPathQuery:tableXpathQueryString];
                    for(TFHppleElement *row in tableNodes){
                        TFHppleElement *child = [row.children objectAtIndex:1];
                        if([child.tagName isEqualToString:@"td"]){
                            NSDictionary *dict = @{
                                                   @"location": [[[child.children objectAtIndex:2] content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                                   @"address":[[[child.children objectAtIndex:6] content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                                   @"arriveTime": [[[[row.children objectAtIndex:2] firstChild] content]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                                   @"departTime": [[[[row.children objectAtIndex:3] firstChild] content]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]
                                                   };
                            [trip.stops addObject:dict];
                        }
                    }
                    
                    //load trip price
                    query = @"//li[@class='five']/p";
                    NSArray *priceNode = [element searchWithXPathQuery:query];
                    [trip setPriceFrom:[[[[priceNode objectAtIndex:0] firstChild] content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
                    tripsFound = YES;
                    trip.carrierID = 1;
                    trip.needReservation = YES;
                    trip.date = self.date;
                    trip.buyURL = url;
                    trip.departLocation = [self.departLocation stringLocation];
                    trip.arriveLocation = [self.arriveLocation stringLocation];
                    trip.departAddress = [[trip.stops objectAtIndex:0] objectForKey:@"address"];
                    trip.arriveAddress = [[trip.stops objectAtIndex:[trip.stops count] - 1] objectForKey:@"address"];
                    [trip setDepartTime:[[trip.stops objectAtIndex:0] objectForKey:@"departTime"] andArriveTime:[[trip.stops objectAtIndex:[trip.stops count] - 1] objectForKey:@"arriveTime"]];
                    
                }
                [self.table performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            }];

    [task resume];
}


- (void)getCoachusaTrips
{
    
    if([coachUSAFares objectForKey:self.departLocation.city] && [coachUSAFares objectForKey:self.arriveLocation.city]){
        NSURL *url = [NSURL URLWithString:
                      [NSString stringWithFormat:@"http://www.coachusa.com/ss.details.asp?action=Lookup&c1=%@&s1=%@&c2=%@&s2=%@",self.departLocation.city, self.departLocation.state, self.arriveLocation.city, self.arriveLocation.state]];
        task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                {
                    //load parser with html
                    parser = [TFHpple hppleWithHTMLData:data];
                    
                    //setup query and parse
                    query = @"//td[@class='tableHeader']";
                    nodes = [parser searchWithXPathQuery:query];
                    
                    //construct stop name array
                    NSMutableArray *stopsArray = [[NSMutableArray alloc] init];
                    int departStops = [[[nodes objectAtIndex:1] objectForKey:@"colspan"] integerValue];
                    int arriveStops = [[[nodes objectAtIndex:2] objectForKey:@"colspan"] integerValue];
                    
                    while(departStops > 0){
                        [stopsArray addObject:[self.departLocation stringLocation]];
                        departStops --;
                    }
                    while(arriveStops > 0){
                        [stopsArray addObject:[self.arriveLocation stringLocation]];
                        arriveStops --;
                    }
                    
                    //construct stop address array
                    query = @"//table[@border='1']/tr";
                    NSArray *tripsNodes = [parser searchWithXPathQuery:query];
                    nodes = [[tripsNodes objectAtIndex:1] searchWithXPathQuery:@"//td[@class='tableHeader']/table/tr/td/img"];
                    
                    NSMutableArray *stopsAddress = [[NSMutableArray alloc] init];
                    
                    for(TFHppleElement *element in nodes){
                        [stopsAddress addObject:[element objectForKey:@"alt"]];
                    }
                    
                    //construct stops and finalize trip
                    for(int i = 2; i < [tripsNodes count]; i++){
                        
                        query = @"//td[@nowrap='True']";
                        NSArray *cells = [[tripsNodes objectAtIndex:i] searchWithXPathQuery:query];
                        
                        if([self.date compare:[NSDate date]] == NSOrderedDescending || [self currentMin] < [self departMin:[[[cells objectAtIndex:0] firstChild] content]]){
                            TripObject *trip = [[TripObject alloc] init];
                            trip.stops = [[NSMutableArray alloc] init];
                            [self.searchedTrips addObject:trip];
                            
                            
                            for(int k = 0; k < [stopsArray count]; k++){
                                NSDictionary *dict = @{@"location":[stopsArray objectAtIndex:k],
                                                       @"address":[stopsAddress objectAtIndex:k],
                                                       @"arriveTime":[[[cells objectAtIndex:k] firstChild] content],
                                                       @"departTime":[[[cells objectAtIndex:k] firstChild] content]
                                                       };
                                
                                [trip.stops addObject:dict];
                            }
                            
                            tripsFound = YES;
                            trip.carrierID = 2;
                            trip.needReservation = NO;
                            trip.date = self.date;
                            trip.buyURL = url;
                            trip.departLocation = [self.departLocation stringLocation];
                            trip.arriveLocation = [self.arriveLocation stringLocation];
                            trip.departAddress = [[trip.stops objectAtIndex:0] objectForKey:@"address"];
                            trip.arriveAddress = [[trip.stops objectAtIndex:[trip.stops count] - 1] objectForKey:@"address"];
                            [trip setDepartTime:[[trip.stops objectAtIndex:0] objectForKey:@"departTime"] andArriveTime:[[trip.stops objectAtIndex:[trip.stops count] - 1] objectForKey:@"arriveTime"]];
                            [trip setPriceFrom:[[coachUSAFares objectForKey:self.departLocation.city] objectForKey:self.arriveLocation.city]];
                        }
                        else continue;
                    }
                    [self.table performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                    if([self.searchedTrips count] == 0)
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                }];
        [task resume];
    }
    //Display "no search results"
    // only if coach usa is the only carrier being viewed
    // and coach usa does not provide service to the input locations
    // otherwise, app will get megabus results
//    else if (!singleton.megabus){
//        //self.noTripsLabel.hidden = NO;
//        self.noTripsView.hidden = NO;
//        self.table.hidden = YES;
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"trip"]){
        
        NSIndexPath *indexPath = [self.table indexPathForSelectedRow];
        TripDetailsViewController *tripView = [segue destinationViewController];
        
        tripView.trip = [self.searchedTrips objectAtIndex:indexPath.row];
        tripView.tripList = self.searchedTrips;
        tripView.index = indexPath.row;
    }
}



#pragma mark - SortPickerDelegate method
-(void)selectedSort:(NSNumber *)newSort
{
    //Dismiss the popover if it's showing.
    if (_sortPickerPopover) {
        [_sortPickerPopover dismissPopoverAnimated:YES];
        _sortPickerPopover = nil;
    }
    
    switch([newSort intValue]){
        case PRICELOWTOHIGH:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [a comparePrice:b];
            }]];
            break;
        case PRICEHIGHTOLOW:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [b comparePrice:a];
            }]];
            break;
        case DURATIONLOWTOHIGH:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [a compareDuration:b];
            }]];
            break;
        case DURATIONHIGHTOLOW:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [b compareDuration:a];
            }]];
            break;
        case DEPARTLOWTOHIGH:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [a compareDepartTime:b];
            }]];
            break;
        case DEPARTHIGHTOLOW:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [b compareDepartTime:a];
            }]];
            break;
        case ARRIVELOWTOHIGH:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [a compareArriveTime:b];
            }]];
            break;
        case ARRIVEHIGHTOLOW:
            self.searchedTrips = [NSMutableArray arrayWithArray:[self.searchedTrips sortedArrayUsingComparator:^NSComparisonResult(TripObject *a, TripObject *b){
                return [b compareArriveTime:a];
            }]];
            break;
        default:
            break;
    }
    
    [self.table performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

#pragma mark - Button action outlets
- (IBAction)decrementDate:(id)sender {
    if([self.date  compare:[[NSDate alloc]init]] == NSOrderedDescending){
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        self.date = [calendar dateByAddingComponents:dayComponent toDate:self.date options:0];
        [self.dateLabel setText:[formatter stringFromDate:self.date]];
        [self loadTrips];
    }
}
- (IBAction)incrementDate:(id)sender {
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    self.date = [calendar dateByAddingComponents:dayComponent toDate:self.date options:0];
    [self.dateLabel setText:[formatter stringFromDate:self.date]];
    [self loadTrips];
}

-(IBAction)sortButtonTapped:(id)sender
{
    //Create the ColorPickerViewController.
    if (_sortPicker == nil) {
        _sortPicker = [[SortPopoverViewController alloc] initWithStyle:UITableViewStylePlain];
        _sortPicker.delegate = self;
    }
    if (_sortPickerPopover == nil) {
        //The color picker popover is not showing. Show it.
        _sortPickerPopover = [[UIPopoverController alloc] initWithContentViewController:_sortPicker];
        [_sortPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        //The color picker popover is showing. Hide it.
        [_sortPickerPopover dismissPopoverAnimated:YES];
        _sortPickerPopover = nil;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchedTrips count];
    
}
- (TripCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(!self.noTripsView.hidden)
//        self.noTripsView.hidden = true;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    //[MBProgressHUD hideAllHUDForView:self.view animated:YES];
    
    TripCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if(cell == nil) cell = [[TripCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    
    if([self.searchedTrips objectAtIndex:indexPath.row] != nil){
        TripObject *trip  = [self.searchedTrips objectAtIndex:indexPath.row];
        switch(trip.carrierID){
            case 1:
                [cell.carrierImage setImage:[UIImage imageNamed:@"megabus"]];
                break;
            case 2:
                [cell.carrierImage setImage:[UIImage imageNamed:@"coachusa"]];
                break;
            default:
                break;
        }
        
        [cell.arriveLocationLabel setText:trip.departLocation];
        [cell.departLocationLabel setText:trip.arriveLocation];
        [cell.arriveTimeLabel setText:[trip stringArriveTime]];
        [cell.departTimeLabel setText:[trip stringDepartTime]];
        [cell.durationLabel setText:[trip stringDuration]];
        [cell.priceLabel setText:[trip stringPrice]];
        [cell.numStopLabel setText:[NSString stringWithFormat:@"%u",[trip.stops count] - 2]];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sort"] style:UIBarButtonItemStyleDone target:self action:@selector(sortButtonTapped:)];
    self.tabBarController.navigationItem.rightBarButtonItem = rightButton;
}

//helper functions
- (int)currentMin
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate:[NSDate date]];
    return [components hour] * 60 + [components minute];
}

- (int) departMin:(NSString *)time
{
    BOOL isdepartAM;
    
    if ([time rangeOfString:@"A" options:NSCaseInsensitiveSearch].location == NSNotFound) isdepartAM= false;
    else isdepartAM = true;
    
    NSArray *departElements = [time componentsSeparatedByCharactersInSet:
                               [NSCharacterSet characterSetWithCharactersInString:@":apmAPM"]];
    
    if(isdepartAM) return 60 * [[departElements objectAtIndex:0] integerValue] + [[departElements objectAtIndex:1] integerValue];
    else{
        if([[departElements objectAtIndex:0] integerValue] == 12)
            return 60 * [[departElements objectAtIndex:0] integerValue] + [[departElements objectAtIndex:1] integerValue];
        else
            return  60 * [[departElements objectAtIndex:0] integerValue] + [[departElements objectAtIndex:1] integerValue] + 720;
    }
}



@end
