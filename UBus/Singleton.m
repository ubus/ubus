//
//  Singleton.m
//  UBus
//
//  Created by Ben Ruble on 5/11/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "Singleton.h"


@implementation Singleton

@synthesize megabus;
@synthesize coach;
@synthesize myTripsArray;

+ (Singleton *) singleton {
    static Singleton *singleton = nil;
    static dispatch_once_t once_token;
    
    dispatch_once(&once_token, ^{
        singleton = [[Singleton alloc] init];
        singleton.myTripsArray = [[NSMutableArray alloc]init];
    });
    return singleton;
}
@end