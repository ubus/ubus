//
//  BusToView.h
//  UBus
//
//  Created by Patong Vang on 5/1/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusToView : NSObject

{
    BOOL megabus;
    BOOL coach;
 }

@property(nonatomic) BOOL megabus;

@property(nonatomic) BOOL coach;

+(BusToView *) singleObj;
@end
