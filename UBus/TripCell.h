//
//  TripCell.h
//  UBus
//
//  Created by Cindy on 4/8/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *departLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocationLabel;

@property (weak, nonatomic) IBOutlet UILabel *departTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *numStopLabel;
@property (weak, nonatomic) IBOutlet UIImageView *carrierImage;

@end
