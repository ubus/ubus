//
//  MyTripsTableViewController.m
//  UBus
//
//  Created by Cindy on 5/10/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "MyTripsTableViewController.h"
#import "Singleton.h"
#import "TripObject.h"
#import "TripCell.h"

@interface MyTripsTableViewController ()
@end

@implementation MyTripsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[Singleton singleton].myTripsArray count];
}


- (TripCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TripCell *cell = [tableView dequeueReusableCellWithIdentifier:@"savedTrip" forIndexPath:indexPath];
    if(cell == nil) cell = [[TripCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"savedTrip"];
    if([[Singleton singleton].myTripsArray objectAtIndex:indexPath.row] != nil){
        TripObject *trip  = [[Singleton singleton].myTripsArray objectAtIndex:indexPath.row];
        switch(trip.carrierID){
            case 1:
                [cell.carrierImage setImage:[UIImage imageNamed:@"megabus"]];
                break;
            case 2:
                [cell.carrierImage setImage:[UIImage imageNamed:@"coachusa"]];
                break;
            default:
                break;
        }
        
        [cell.arriveLocationLabel setText:trip.departLocation];
        [cell.departLocationLabel setText:trip.arriveLocation];
        [cell.arriveTimeLabel setText:[trip stringArriveTime]];
        [cell.departTimeLabel setText:[trip stringDepartTime]];
        [cell.durationLabel setText:[trip stringDuration]];
        [cell.priceLabel setText:[trip stringPrice]];
        [cell.numStopLabel setText:[NSString stringWithFormat:@"%u",[trip.stops count] - 2]];
    }
    NSLog(@"im here");
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"trip"]){
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        TripDetailsViewController *tripView = [segue destinationViewController];
        
        tripView.trip = [[Singleton singleton].myTripsArray objectAtIndex:indexPath.row];
        tripView.tripList = [Singleton singleton].myTripsArray;
        tripView.index = indexPath.row;
    }
}

 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
     // Return NO if you do not want the specified item to be editable.
     return YES;
 }
 


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         // Delete the row from the data source
         //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
         [[Singleton singleton].myTripsArray removeObjectAtIndex:indexPath.row];
         [tableView reloadData];
     }
 }
 

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
