//
//  Singleton.h
//  UBus
//
//  Created by Ben Ruble on 5/11/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject 

@property(nonatomic) BOOL megabus;
@property(nonatomic) BOOL coach;
@property (nonatomic, retain) NSMutableArray *myTripsArray;

+ (Singleton *) singleton;

@end
