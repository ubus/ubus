//
//  StopCell.h
//  UBus
//
//  Created by Cindy on 5/7/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UILabel *departTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveTimeLabel;

@end
