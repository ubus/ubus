//
//  EditBusViewController.h
//  UBus
//
//  Created by Patong Vang on 4/29/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@protocol StateSelectionDelegate <NSObject>//*
-(void) selectedState:(NSString *)state forNation:(NSString *)nation;//*
@end//*

@interface EditBusViewController : UITableViewController
{
    Singleton *singleton;
  
}

//delegate
@property (nonatomic, weak) id <StateSelectionDelegate> delegate;//*


//switch
@property (weak, nonatomic) IBOutlet UISwitch *switchMegabus;
@property (weak, nonatomic) IBOutlet UISwitch *switchCoach;

//toggle
- (IBAction)toggleMegabus:(id)sender;
- (IBAction)toggleCoach:(id)sender;

//bool





@end
