//
//  TripDetailsViewController.m
//  UBus
//
//  Created by Cindy on 4/11/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "TripDetailsViewController.h"
#import "TripViewController.h"
#import "Singleton.h"
#import "StopCell.h"


@interface TripDetailsViewController ()
@end

@implementation TripDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _containerView.layer.cornerRadius = 15;
    _containerView.layer.masksToBounds = YES;

	//add bottom bar
	UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 44, self.view.bounds.size.width, 44)];
	toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
	[toolbar setItems:@[[[UIBarButtonItem alloc] initWithTitle:@"Buy" style:UIBarButtonItemStyleBordered target:self action:@selector(buy:)],
                        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL],
                        [[UIBarButtonItem alloc] initWithTitle:@"Stop Details" style:UIBarButtonItemStyleBordered target:self action:@selector(stopDetails:)],
                        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL],
                        [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(save:)]]];
	[self.view addSubview:toolbar];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"detail"]){
        TripViewController *tripView = [segue destinationViewController];
        tripView.trip = self.trip;
        [tripView.view setNeedsDisplay];
    }
}

-(IBAction)buy:(id)sender
{
    [[UIApplication sharedApplication] openURL:self.trip.buyURL];
}
                                                                                                            
-(IBAction)save:(id)sender
{
    BOOL exist = false;
    for(int i = 0; i < [[Singleton singleton].myTripsArray count]; i++ ){
        if([self.trip compare:[[Singleton singleton].myTripsArray objectAtIndex:i]])
            exist = true;
    }
    if(!exist){
        [[Singleton singleton].myTripsArray addObject:self.trip];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" 
                                                        message:@"This trip was saved before!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)stopDetails:(id)sender
{
    if (_stopDetails == nil) {
        _stopDetails = [[StopDetailsPopoverViewController alloc] initWithStyle:UITableViewStylePlain];
         _stopDetails.stops = _trip.stops;
    }
    if (_stopDetailsPopover == nil) {
        _stopDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:_stopDetails];
        [_stopDetailsPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_stopDetailsPopover dismissPopoverAnimated:YES];
        _stopDetailsPopover = nil;
    }

}
                                                                                                                                      
@end

