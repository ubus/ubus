//
//  StopCell.m
//  UBus
//
//  Created by Cindy on 5/7/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "StopCell.h"

@implementation StopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:@"stopCell"];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSString *) reuseIdentifier {
    return @"stopCell";
}
@end
