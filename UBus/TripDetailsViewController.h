//
//  TripDetailsViewController.h
//  UBus
//
//  Created by Cindy on 4/11/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "StopDetailsPopoverViewController.h"
#import "TripObject.h"


@interface TripDetailsViewController : UIViewController 

@property int index;
@property (strong, nonatomic) TripObject *trip;
@property (strong, nonatomic) NSMutableArray *tripList;

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (nonatomic, strong) StopDetailsPopoverViewController *stopDetails;
@property (nonatomic, strong) UIPopoverController *stopDetailsPopover;
@end
