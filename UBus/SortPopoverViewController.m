//
//  SortPopoverViewController.m
//  UBus
//
//  Created by Cindy on 4/23/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "SortPopoverViewController.h"

@interface SortPopoverViewController ()

@end

@implementation SortPopoverViewController{
    NSDictionary *sortDict;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    if ([super initWithStyle:style] != nil) {
        
        //Initialize the array
        self.sortFunctions = [NSMutableArray array];
        
        //Set up the array of colors.
        [self.sortFunctions addObject:@PRICELOWTOHIGH];
        [self.sortFunctions addObject:@PRICEHIGHTOLOW];
        [self.sortFunctions addObject:@DURATIONLOWTOHIGH];
        [self.sortFunctions addObject:@DURATIONHIGHTOLOW];
        [self.sortFunctions addObject:@DEPARTLOWTOHIGH];
        [self.sortFunctions addObject:@DEPARTHIGHTOLOW];
        [self.sortFunctions addObject:@ARRIVELOWTOHIGH];
        [self.sortFunctions addObject:@ARRIVEHIGHTOLOW];
        
        sortDict = @{
                     @PRICELOWTOHIGH : @"Price: Ascending",
                     @PRICEHIGHTOLOW : @"Price: Descending",
                     @DURATIONLOWTOHIGH : @"Duration: Ascending",
                     @DURATIONHIGHTOLOW : @"Duration: Descending",
                     @DEPARTLOWTOHIGH: @"Depart Time: Ascending",
                     @DEPARTHIGHTOLOW: @"Depart Time: Descending",
                     @ARRIVELOWTOHIGH: @"Arrive Time: Ascending",
                     @ARRIVEHIGHTOLOW: @"Arrive Time: Descending"
                   };
        
        //Make row selections persist.
        self.clearsSelectionOnViewWillAppear = NO;
        
        //Calculate how tall the view should be by multiplying 
        //the individual row height by the total number of rows.
        NSInteger rowsCount = [self.sortFunctions count];
        NSInteger singleRowHeight = [self.tableView.delegate tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        NSInteger totalRowsHeight = rowsCount * singleRowHeight;
        
        //Calculate how wide the view should be by finding how 
        //wide each string is expected to be
        CGFloat largestLabelWidth = 0;
        for (NSNumber *function in self.sortFunctions) {
            //Checks size of text using the default font for UITableViewCell's textLabel. 
            CGSize labelSize = [[sortDict objectForKey:function] sizeWithFont:[UIFont systemFontOfSize:13.0f]];
            if (labelSize.width > largestLabelWidth) {
                largestLabelWidth = labelSize.width;
            }
        }
        
        //Add a little padding to the width
        CGFloat popoverWidth = largestLabelWidth + 30;
        
        //Set the property to tell the popover container how big this view will be.
        self.preferredContentSize = CGSizeMake(popoverWidth, totalRowsHeight);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sortFunctions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [sortDict objectForKey:[self.sortFunctions objectAtIndex:indexPath.row]];
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor redColor];
    bgColorView.layer.cornerRadius = 7;
    bgColorView.layer.masksToBounds = YES;
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *selectedSort = [self.sortFunctions objectAtIndex:indexPath.row];
    
    //Notify the delegate if it exists.
    if (_delegate != nil) {
        [_delegate selectedSort:selectedSort];
    }
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
