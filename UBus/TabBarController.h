//
//  TabBarController.h
//  UBus
//
//  Created by Cindy on 4/20/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationObject.h"

@interface TabBarController : UITabBarController

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) LocationObject *departLocation;
@property (nonatomic, strong) LocationObject *arriveLocation;

@end
