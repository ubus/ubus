//
//  LocationObject.m
//  UBus
//
//  Created by Zhiqing Zhang on 4/11/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "LocationObject.h"

@implementation LocationObject

- (NSString *) stringLocation
{
    return [NSString stringWithFormat:@"%@, %@", self.city, self.state];
}


+ (LocationObject *) createObjectWithCity: (NSString *)city andState: (NSString *)state andMegabusCode: (NSNumber *)code
{
    LocationObject *loc = [[LocationObject alloc] init];
    loc.city = city;
    loc.state = state;
    loc.megabusCode = code;
    
    return loc;
}

- (BOOL) compare:(LocationObject *)location
{
    return [self.city compare:location.city] == NSOrderedSame && [self.state compare:location.state] == NSOrderedSame;
    
}

@end
