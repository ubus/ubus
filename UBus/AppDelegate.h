//
//  AppDelegate.h
//  UBus
//
//  Created by Cindy on 4/8/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIButton *button;
@property (strong, nonatomic) UINavigationBar *navbar;
@property (strong, nonatomic) UITabBar *tabbar;
@property (strong, nonatomic) UITabBarItem *tabbaritem;
@property (strong, nonatomic) UITabBarController *tabcontroller;

@end
