//
//  TripViewController.h
//  UBus
//
//  Created by Cindy on 4/24/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "TripDetailsViewController.h"
#import "TripObject.h"

@interface TripViewController : UIViewController

@property BOOL displayingFront;

@property (strong, nonatomic) TripObject *trip;
@property (strong, nonatomic) CLGeocoder *geocoder;

@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *departLocation;
@property (weak, nonatomic) IBOutlet UILabel *departAddress;
@property (weak, nonatomic) IBOutlet UILabel *departTime;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocation;
@property (weak, nonatomic) IBOutlet UILabel *arriveAddress;
@property (weak, nonatomic) IBOutlet UILabel *arrivalTime;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *reservation;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIImageView *carrierImage;

@property (weak, nonatomic)  IBOutlet UIView *mapView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mapSegment;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UILabel *address;

- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender;
- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender;

@end
