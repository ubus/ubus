//
//  StopDetailsPopoverViewController.h
//  UBus
//
//  Created by Cindy on 5/7/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopDetailsPopoverViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *stops;

@end
