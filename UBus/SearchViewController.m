//
//  SearchViewController.m
//  UBus
//
//  Created by Cindy on 4/8/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "SearchViewController.h"
#import "TabBarController.h"
#import "LocationObject.h"


@interface SearchViewController (){
    
    LocationObject *departLocation;
    LocationObject *arriveLocation;
    NSDictionary *locationDict;
    BOOL arriveLocSelected;
    BOOL departLocSelected;
    
    BOOL differentLocations;
    
}
@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arriveLocSelected = NO;
    departLocSelected = NO;
    differentLocations = NO;
    
    singleton = [Singleton singleton];
    singleton.megabus = YES;
    singleton.coach = YES;
    
    self.departLocationList = [[NSMutableArray alloc] init];
    self.arriveLocationList = [[NSMutableArray alloc] init];
    
	// Do any additional setup after loading the view, typically from a nib.
    departLocation = [[LocationObject alloc] init];
    arriveLocation = [[LocationObject alloc] init];

    [self initAll];
    
    self.departLocationPickerContainer.frame = CGRectMake(320, 300, 320, 261);
    self.arriveLocationPickerContainer.frame = CGRectMake(320, 300, 320, 261);
    self.datePickerContainer.frame = CGRectMake(0, 300, 320, 261);
    
    [self.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    
}

- (void) initAll
{
     locationDict = @{
                                  @"Madison, WI" : @{
                                           @"city" : @"Madison",
                                           @"state": @"WI",
                                           @"megabusCode":[NSNumber numberWithInt:119]
                                           },
                                   @"Chicago, IL" : @{
                                           @"city":@"Chicago",
                                           @"state":@"IL",
                                           @"megabusCode":[NSNumber numberWithInt:100]
                                           },
                                   @"UW Madison, WI" : @{
                                           @"city":@"U of Wisc Madison",
                                           @"state":@"WI",
                                           @"megabusCode":[NSNumber numberWithInt:300]
                                           },
                                  @"Milwaukee, WI" : @{
                                          @"city":@"Milwaukee",
                                          @"state":@"WI",
                                          @"megabusCode":[NSNumber numberWithInt:121]
                                          },
                                   @"Minneapolis, MN" : @{
                                           @"city":@"Minneapolis",
                                           @"state":@"MN",
                                           @"megabusCode":[NSNumber numberWithInt:144]
                                           },
                                  @"St. Paul, MN" : @{
                                          @"city":@"St. Paul",
                                          @"state":@"MN",
                                          @"megabusCode":[NSNumber numberWithInt:430]
                                          },
                                  @"Ann Arbor, MI" : @{
                                          @"city":@"Ann Arbor",
                                          @"state":@"MI",
                                          @"megabusCode":[NSNumber numberWithInt:91]
                                          },
                                  @"East Lansing, MI" : @{
                                          @"city":@"East Lansing",
                                          @"state":@"MI",
                                          @"megabusCode":[NSNumber numberWithInt:330]
                                          },
                                @"Grand Rapids, MI" : @{
                                          @"city":@"Grand Rapids",
                                          @"state":@"MI",
                                          @"megabusCode":[NSNumber numberWithInt:331]
                                        },
                                  @"Detroit, MI" : @{
                                          @"city":@"Detroit",
                                          @"state":@"MI",
                                          @"megabusCode":[NSNumber numberWithInt:107]
                                          },
                                  @"Des Moines, IA" : @{
                                          @"city":@"Des Moines",
                                          @"state":@"IA",
                                          @"megabusCode":[NSNumber numberWithInt:106]
                                          },
                                  @"Iowa City, IA" : @{
                                          @"city":@"Iowa City",
                                          @"state":@"IA",
                                          @"megabusCode":[NSNumber numberWithInt:106]
                                          },
                                  @"Indianapolis, IN" : @{
                                          @"city":@"Indianapolis",
                                          @"state":@"IN",
                                          @"megabusCode":[NSNumber numberWithInt:115]
                                          },
                                  
                    };
    
    NSArray *locKeys = [locationDict allKeys];
    
    for(int i = 0; i < [locKeys count]; i ++){
        NSDictionary *tmp = [locationDict objectForKey:[locKeys objectAtIndex:i]];
        [self.departLocationList addObject:[LocationObject createObjectWithCity:[tmp objectForKey:@"city"] andState:[tmp objectForKey:@"state"] andMegabusCode:[tmp objectForKey:@"megabusCode"]]];
        [self.arriveLocationList addObject:[LocationObject createObjectWithCity:[tmp objectForKey:@"city"] andState:[tmp objectForKey:@"state"] andMegabusCode:[tmp objectForKey:@"megabusCode"]]];
    }
}




#pragma system and segues
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"tab"]){
        TabBarController *tabBar = [segue destinationViewController];
        [tabBar setDate:self.datePicker.date];
        tabBar.departLocation = departLocation;
        tabBar.arriveLocation = arriveLocation;
    }
}

#pragma picker functions
- (void) datePickerChanged: (UIDatePicker *) datePicker{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    [self.dateLabel setText:strDate];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if (pickerView == self.departLocationPicker)
        return [self.departLocationList count];
    else
        return [self.arriveLocationList count];
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    LocationObject *tmp = [[LocationObject alloc] init];
    if (pickerView == self.departLocationPicker)
        tmp = [self.departLocationList objectAtIndex:row];
    else
        tmp = [self.arriveLocationList objectAtIndex:row];
     return [tmp stringLocation];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == self.departLocationPicker){
        departLocation = [self.departLocationList objectAtIndex:row];
        [self.departLocationLabel setText:[departLocation stringLocation]];
        self.datePickerButton.frame = CGRectMake(0,5,200,200);
        departLocSelected = YES;
    }
    else{
        arriveLocation = [self.arriveLocationList objectAtIndex:row];
        [self.arriveLocationLabel setText:[arriveLocation stringLocation]];
        arriveLocSelected = YES;
    }
    differentLocations = ![departLocation compare:arriveLocation];
}

- (IBAction)showDepartLocationPicker:(id)sender {
    [self hideArriveLocationPicker:sender];
    [self hideDatePicker:sender];
    self.departLocationPickerContainer.frame = CGRectMake(0, 250, 320, 196);
}
- (IBAction)hideDepartLocationPicker:(id)sender {
    self.departLocationPickerContainer.frame = CGRectMake(320, 0, 320, 196);
}
- (IBAction)showArriveLocationPicker:(id)sender {
    [self hideDepartLocationPicker:sender];
    [self hideDatePicker:sender];
    self.arriveLocationPickerContainer.frame = CGRectMake(0, 250, 320, 196);
}
- (IBAction)hideArriveLocationPicker:(id)sender {
    self.arriveLocationPickerContainer.frame = CGRectMake(320, 0, 320, 196);
}
- (IBAction)hideDatePicker:(id)sender {
    self.datePickerContainer.frame = CGRectMake(320, 0, 320, 196);
}
- (IBAction)showDatePicker:(id)sender {
    [self hideArriveLocationPicker:sender];
    [self hideDepartLocationPicker:sender];
    self.datePickerContainer.frame = CGRectMake(0, 250, 320, 196);
    
}

- (IBAction)lookUpBtnPressed:(id)sender {
   
   
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"settings"]){
        return YES;
    }
    else{
        BOOL dateReady = [identifier isEqualToString:@"tab"] && ([self.datePicker.date timeIntervalSinceNow] > 0.0);
        BOOL arriveLocationReady = [identifier isEqualToString:@"tab"] && arriveLocSelected && differentLocations;
        BOOL departLocationReady = [identifier isEqualToString:@"tab"] && departLocSelected && differentLocations;
    
        if (!dateReady){
            [self startShake:self.dateContainer];
        }
        if (!arriveLocationReady){
            [self startShake:self.arriveContainer];
        }
        if(!departLocationReady){
            [self startShake:self.departContainer];
        }
    
        return arriveLocationReady && dateReady && departLocationReady;
    }
}

- (void) startShake:(UIView*)view
{
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-6, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(6, 0);
    
    view.transform = leftShake;
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake;
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}
@end
