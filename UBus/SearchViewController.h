//
//  SearchViewController.h
//  UBus
//
//  Created by Cindy on 4/8/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditBusViewController.h"
#import "Singleton.h"

@interface SearchViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate>{
    Singleton *singleton;
}

//buttons
@property (weak, nonatomic)  IBOutlet UIButton *searchButton;
@property (weak, nonatomic)  IBOutlet UIButton *departLocationPickerButton;
@property (weak, nonatomic)  IBOutlet UIButton *arriveLocationPickerButton;
@property (weak, nonatomic)  IBOutlet UIButton *datePickerButton;
- (IBAction)lookUpBtnPressed:(id)sender;

//button view containers
@property (weak, nonatomic) IBOutlet UIView *dateContainer;
@property (weak, nonatomic) IBOutlet UIView *departContainer;
@property (weak, nonatomic) IBOutlet UIView *arriveContainer;

//labels
@property (weak, nonatomic) IBOutlet UILabel *departLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;


//picker views and their containers
@property (weak, nonatomic) IBOutlet UIView *departLocationPickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *departLocationPicker;
- (IBAction)showDepartLocationPicker:(id)sender;
- (IBAction)hideDepartLocationPicker:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *arriveLocationPickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *arriveLocationPicker;
- (IBAction)showArriveLocationPicker:(id)sender;
- (IBAction)hideArriveLocationPicker:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *datePickerContainer;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)showDatePicker:(id)sender;
- (IBAction)hideDatePicker:(id)sender;

//setting button
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsIcon;


//array of locations
@property (strong, nonatomic) NSMutableArray *departLocationList;
@property (strong, nonatomic) NSMutableArray *arriveLocationList;

@end
