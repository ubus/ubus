//
//  TripObject.m
//  UBus
//
//  Created by Cindy on 4/8/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import "TripObject.h"

@implementation TripObject

- (void) setDepartTime:(NSString *)departTime andArriveTime:(NSString *)arriveTime
{
    BOOL isdepartAM, isarriveAM;
    
    if ([departTime rangeOfString:@"A" options:NSCaseInsensitiveSearch].location == NSNotFound) isdepartAM= false;
    else isdepartAM = true;
    
    if ([arriveTime rangeOfString:@"A" options:NSCaseInsensitiveSearch].location == NSNotFound) isarriveAM= false;
    else isarriveAM = true;
    
    NSArray *departElements = [departTime componentsSeparatedByCharactersInSet:
                         [NSCharacterSet characterSetWithCharactersInString:@":apmAPM"]];
    NSArray *arriveElements = [arriveTime componentsSeparatedByCharactersInSet:
                               [NSCharacterSet characterSetWithCharactersInString:@":apmAPM"]];
    
    
    if(isdepartAM) self.departTime = 60 * [[departElements objectAtIndex:0] integerValue] + [[departElements objectAtIndex:1] integerValue];
    else{
        if([[departElements objectAtIndex:0] integerValue] == 12)
            self.departTime = 60 * [[departElements objectAtIndex:0] integerValue] + [[departElements objectAtIndex:1] integerValue];
        else
            self.departTime = 60 * [[departElements objectAtIndex:0] integerValue] + [[departElements objectAtIndex:1] integerValue] + 720;
    }
    
    if(isarriveAM){
        if(!isdepartAM){
            if([[arriveElements objectAtIndex:0] integerValue] == 12)
                self.arriveTime = 24 * 60 + [[arriveElements objectAtIndex:1] integerValue];
            else
                self.arriveTime = 24 * 60 + [[arriveElements objectAtIndex:0] integerValue] * 60 + [[arriveElements objectAtIndex:1] integerValue];
        }else
            self.arriveTime = [[arriveElements objectAtIndex:0] integerValue] * 60 + [[arriveElements objectAtIndex:1] integerValue];
    }else{
        if([[arriveElements objectAtIndex:0] integerValue] == 12)
            self.arriveTime = 60 * [[arriveElements objectAtIndex:0] integerValue] + [[arriveElements objectAtIndex:1] integerValue];
        else
            self.arriveTime = 60 * [[arriveElements objectAtIndex:0] integerValue] + [[arriveElements objectAtIndex:1] integerValue] + 720;
    }
        
    self.duration = self.arriveTime - self.departTime;
}




- (void) setPriceFrom:(NSString *)price
{
    
    if([price rangeOfString:@"$"].location == NSNotFound)
        self.price = [price doubleValue];
    else{
        NSArray *priceElements = [price componentsSeparatedByCharactersInSet:
                                  [NSCharacterSet characterSetWithCharactersInString:@"$"]];
        self.price = [[priceElements objectAtIndex:1] doubleValue];
    }
}
    
- (NSString *) stringDepartTime
{
    int selector = self.departTime/60;
    if(selector > 12)
        return [NSString stringWithFormat:@"%d:%02d PM", selector - 12, self.departTime%60];
    else if(selector == 12)
        return [NSString stringWithFormat:@"%d:%02d PM", selector, self.departTime%60];
    else
        return [NSString stringWithFormat:@"%d:%02d AM", selector, self.departTime%60];
}

- (NSString *) stringArriveTime
{
    int selector = self.arriveTime/60;
    if(selector >= 24)
        return [NSString stringWithFormat:@"%d:%02d AM", selector - 24, self.arriveTime%60];
    else if(selector > 12)
        return [NSString stringWithFormat:@"%d:%02d PM", selector - 12, self.arriveTime%60];
    else if(selector == 12)
        return [NSString stringWithFormat:@"%d:%02d PM", selector, self.arriveTime%60];
    else
        return [NSString stringWithFormat:@"%d:%02d AM", selector, self.arriveTime%60];
}

- (NSString *) stringDuration
{
    return [NSString stringWithFormat:@"%d hr %d min", self.duration/60, self.duration%60];
    
}

- (NSString *) stringPrice
{
    return [NSString stringWithFormat:@"$%.2f", self.price];
}

- (NSComparisonResult)comparePrice:(TripObject *)anotherTrip
{
    if(self.price == anotherTrip.price)
        return NSOrderedSame;
    else if(self.price > anotherTrip.price)
        return NSOrderedDescending;
    else return NSOrderedAscending;
}

- (NSComparisonResult)compareDuration:(TripObject *)anotherTrip
{
    if(self.duration == anotherTrip.duration)
        return NSOrderedSame;
    else if(self.duration > anotherTrip.duration)
        return NSOrderedDescending;
    else return NSOrderedAscending;
}

- (NSComparisonResult)compareDepartTime:(TripObject *)anotherTrip
{
    if(self.departTime == anotherTrip.departTime)
        return NSOrderedSame;
    else if(self.departTime > anotherTrip.departTime)
        return NSOrderedDescending;
    else return NSOrderedAscending;
}
- (NSComparisonResult)compareArriveTime:(TripObject *)anotherTrip
{
    if(self.arriveTime == anotherTrip.arriveTime)
        return NSOrderedSame;
    else if(self.arriveTime > anotherTrip.arriveTime)
        return NSOrderedDescending;
    else return NSOrderedAscending;
}

- (BOOL)compare:(TripObject *)anotherTrip
{
    
    return [self compareDepartTime:anotherTrip] == NSOrderedSame && [self compareArriveTime:anotherTrip] == NSOrderedSame && [self comparePrice:anotherTrip] == NSOrderedSame;
        
}
@end
