//
//  LocationObject.h
//  UBus
//
//  Created by Zhiqing Zhang on 4/11/14.
//  Copyright (c) 2014 zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationObject : NSObject

@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;

//@property (strong, nonatomic) NSString *address;


@property (strong, nonatomic) NSNumber *megabusCode;

- (NSString *)stringLocation;
- (BOOL) compare:(LocationObject *)location;
+ (LocationObject *) createObjectWithCity: (NSString *)city andState: (NSString *)state andMegabusCode: (NSNumber *)code;

@end
